package uk.vanleersum.arrowgrid;

import java.util.Random;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

public class ArrowGridMain extends Application implements ArrowGrid.Delegate {
	public static void main(final String[] args) {
		Application.launch(args);
	}
	private Direction directions[][];
	private ArrowGrid arrowGrid;
	private final int gridSize = 10;

	private Point currentPoint = null;


	@Override
	public void clicked(final int x, final int y) {
		System.out.println("ArrowGridMain.clicked: "); // DEBUG
		if (null == this.currentPoint) {
			this.currentPoint = new Point(x, y);
			final Direction direction = this.directions[this.currentPoint.x][this.currentPoint.y];
			this.arrowGrid.setImage(direction.image(false), this.currentPoint.x, this.currentPoint.y);
		}
	}

	@Override
	public void start(final Stage primaryStage) {
		try {
			final int cellSize = 30;
			this.directions = new Direction[this.gridSize][this.gridSize];

			final Button randomiseButton = new Button("Randomise");
			TextField randomText = new TextField("");
			final Button stepButton = new Button("Step");
			Pane filler = new Pane();
			HBox.setHgrow(filler, Priority.ALWAYS);
			final HBox buttonBar = new HBox(stepButton, filler, randomiseButton, randomText);

			this.arrowGrid = new ArrowGrid(this, this.gridSize, cellSize);
			final BorderPane root = new BorderPane(new ScrollPane(this.arrowGrid), null, null, buttonBar, null);
			final Scene scene = new Scene(root, 400, 400);

			
			scene.getStylesheets().add(this.getClass().getResource("application.css").toExternalForm());

			this.randomise(randomText.getText().hashCode());


			randomiseButton.setOnAction(e -> this.randomise(randomText.getText().hashCode()));
			stepButton.setOnAction(e -> this.step());
			
			primaryStage.setScene(scene);
			primaryStage.show();




		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	private final void randomise(long seed) {
		final Random random = new Random(seed);
		this.currentPoint = null;
		for (int x = 0; x < this.gridSize; x++) {
			for (int y = 0; y < this.gridSize; y++) {
				final Direction direction = Direction.values()[random.nextInt(4)];
				this.directions[x][y] = direction;
				this.arrowGrid.setImage(direction.image(true), x, y);
			}
		}
	}

	private final void step() {
		if (null != this.currentPoint) {
			Direction direction = this.directions[this.currentPoint.x][this.currentPoint.y];
			final Point newPoint = direction.nextPoint(this.currentPoint);
			final Direction newDirection = direction.nextDirection();
			this.directions[this.currentPoint.x][this.currentPoint.y] = newDirection;
			this.arrowGrid.setImage(newDirection.image(true), this.currentPoint.x, this.currentPoint.y);

			if ((newPoint.x < 0) || (newPoint.x >= this.gridSize) || (newPoint.y < 0) || (newPoint.y >= this.gridSize)) {
				System.out.println("ArrowGridMain.step: FINISHED"); // DEBUG
				this.currentPoint = null;
				return;
			}
			this.currentPoint = newPoint;
			direction = this.directions[this.currentPoint.x][this.currentPoint.y];
			this.arrowGrid.setImage(direction.image(false), this.currentPoint.x, this.currentPoint.y);
		}
	}

}
