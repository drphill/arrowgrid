package uk.vanleersum.arrowgrid;

public final class Point {
	public final int x,y;

	public Point(final int x, final int y) {
		super();
		this.x = x;
		this.y = y;
	}

}