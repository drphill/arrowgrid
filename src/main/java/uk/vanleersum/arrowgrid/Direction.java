package uk.vanleersum.arrowgrid;

import javafx.scene.image.Image;

enum Direction {
	Up, Right, Down, Left;

	private static final Image RedUp = new Image(Direction.class.getResourceAsStream("redup.png"));
	private static final Image RedRight = new Image(Direction.class.getResourceAsStream("redright.png"));
	private static final Image RedDown = new Image(Direction.class.getResourceAsStream("reddown.png"));
	private static final Image RedLeft = new Image(Direction.class.getResourceAsStream("redleft.png"));

	private static final Image GreenUp = new Image(Direction.class.getResourceAsStream("greenup.png"));
	private static final Image GreenRight = new Image(Direction.class.getResourceAsStream("greenright.png"));
	private static final Image GreenDown = new Image(Direction.class.getResourceAsStream("greendown.png"));
	private static final Image GreenLeft = new Image(Direction.class.getResourceAsStream("greenleft.png"));

	public final Image image(final boolean empty) {
		switch (this) {
			case Down:
				return empty ? Direction.GreenDown : Direction.RedDown;
			case Left:
				return empty ? Direction.GreenLeft : Direction.RedLeft;
			case Right:
				return empty ? Direction.GreenRight : Direction.RedRight;
			default:
			case Up:
				return empty ? Direction.GreenUp : Direction.RedUp;
		}
	}

	public final Direction nextDirection() {
		switch (this) {
			case Down:
				return Left;
			case Left:
				return Up;
			case Right:
				return Down;
			default:
			case Up:
				return Right;
		}
	}

	public final Point nextPoint(final Point pt) {
		switch (this) {
			case Down:
				return new Point(pt.x, pt.y + 1);
			case Left:
				return new Point(pt.x - 1, pt.y);
			case Right:
				return new Point(pt.x + 1, pt.y);
			default:
			case Up:
				return new Point(pt.x, pt.y - 1);
		}
	}
}