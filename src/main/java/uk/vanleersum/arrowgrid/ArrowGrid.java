package uk.vanleersum.arrowgrid;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

public class ArrowGrid extends GridPane {
	public interface Delegate {
		public abstract void clicked(int x, int y);
	}

	private final ImageView[][] imageViews;

	public ArrowGrid(final Delegate delegate, final int gridSize, final int cellSize) {
		this.imageViews = new ImageView[gridSize][gridSize];
		this.setHgap(4);
		this.setVgap(4);
		for (int x = 0; x < gridSize; x++) {
			for (int y = 0; y < gridSize; y++) {
				final ImageView imageView = new ImageView();
				this.imageViews[x][y] = imageView;
				this.add(imageView, x, y);
				imageView.setImage(Direction.Up.image(true));
				imageView.setFitWidth(cellSize);
				imageView.setPreserveRatio(true);
				imageView.setSmooth(true);
				imageView.setCache(true);

				final int xx = x;
				final int yy = y;
				imageView.setOnMouseClicked(e -> {
					if (null != delegate) {
						delegate.clicked(xx, yy);
					}
				});
			}
		}
	}

	public final void setImage(final Image image, final int x, final int y) {
		this.imageViews[x][y].setImage(image);
	}
}
